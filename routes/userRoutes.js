// dependencies
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// router.post("/registerAdmin", (req, res) => {
// 	userController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController))
// })


// Route for user log in
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// A
router.post("/details", (req, res) => {
	userController.getProfileA({userId : req.body.id}).then(resultFromController => res.send(resultFromController))
});

// B
router.get("/details/:id", (req, res) => {
	userController.getProfileB(req.params.id).then(result => res.send(result))
});

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


// Route for enrolling an authenticated user
router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		// User ID will be retrieved from the request header
		userId : auth.decode(req.headers.authorization).id,
		// Course ID will be retrieved from the request body
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));

});


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;