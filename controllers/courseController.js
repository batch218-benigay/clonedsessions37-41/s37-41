const mongoose = require("mongoose");
const Course = require('../models/Course');

// module.exports.addCourse = (reqBody) => {

// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	})
	
// 	return newCourse.save().then((newCourse, error) => {
// 		if(error){
// 			return error;
// 		}
// 		else{
// 			return newCourse;
// 		}
// 	})
// }


// Creating a new course
	module.exports.addCourse = (data) => {
		console.log(data.isAdmin)

		if(data.isAdmin) {
			let newCourse = new Course({
				name: data.course.name,
				description: data.course.description,
				price: data.course.price
			});

			return newCourse.save().then((newCourse, error) => {
				if(error){
					return error
				}
				return newCourse 
			})
		};

		// If the user is not admin, then return this message as a promise to avoid errors
		let message = Promise.resolve('User must be ADMIN to access this.')

		return message.then((value) => {
			return {value}
		})
	};


// Retriving All Courses
module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
		return result;
	})
};


// Retrieve All Active Courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive:true}).then(result => {
			return result;
	})
};

// Retrieve a Specific Course
module.exports.getCourse = (courseId) => {

//inside the parenthesis should be the id
	return Course.findById(courseId).then(result => {
		return result;
	})
};


// Update a Specific Course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		// update code
		return Course.findByIdAndUpdate(courseId , 
			{			//req.body  
				name: newData.course.name,
				description: newData.course.description,
				price: newData.course.price
			}
		).then((updatedCourse, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
};


// Archive a Specific Course
module.exports.archiveCourse = (courseId) => {
	return Course.findByIdAndUpdate(courseId, {
		isActive: false
	})
	.then((archivedCourse, error) => {
		if(error){
			return false
		} 
		return {
			message: "Course archived successfully!"
		}
	})
};


